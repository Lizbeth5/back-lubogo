<?php

$container = $app->getContainer();

// view renderer
$container['renderer'] = function ($c) {
    $settings = $c->get('settings')['renderer'];
    return new Slim\Views\PhpRenderer($settings['template_path']);
};

// monolog
$container['logger'] = function ($c) {
    $settings = $c->get('settings')['logger'];
    $logger = new Monolog\Logger($settings['name']);
    $logger->pushProcessor(new Monolog\Processor\UidProcessor());
    $logger->pushHandler(new Monolog\Handler\StreamHandler($settings['path'], Monolog\Logger::DEBUG));
    return $logger;
};

// Database
$container['db'] = function($c){
    $connectionString = $c->get('settings')['connectionString'];

    $pdo = new PDO($connectionString['dns'], $connectionString['user'], $connectionString['pass']);

    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);

    return new FluentPDO($pdo);
};

// Models
$container['model'] = function($c){
    return (object)[
        'auth' => new App\Model\AuthModel($c->db),
        'user' => new App\Model\UserModel($c->db),
        'vehicle' => new App\Model\VehicleModel($c->db),
        'address' => new App\Model\AddressModel($c->db),
        'trolley' => new App\Model\TrolleyModel($c->db),
        'city' => new App\Model\CityModel($c->db),
        'outstanding' => new App\Model\OutstandingModel($c->db),
        'establishment' => new App\Model\EstablishmentModel($c->db),
        'guarded' => new App\Model\GuardedModel($c->db),
        'product' => new App\Model\ProductModel($c->db),
        'rate' => new App\Model\RateModel($c->db),
        'transaction' => new App\Model\TransactionModel($c->db),
        'travel' => new App\Model\TravelModel($c->db),
        'image' => new App\Model\ImageModel($c->db),
        'offert' => new App\Model\OffertModel($c->db),
        'schedule' => new  App\Model\ScheduleModel($c->db),
        'car' => new App\Model\CarModel($c->db)
     ];
};

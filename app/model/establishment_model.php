<?php 
namespace App\Model;

use App\Lib\Response,
	App\Lib\Cifrado;

/**
 * 
 */
class EstablishmentModel
{
	private $db;
	private $response;
	private $table = 'establecimiento';
	private $tableTypeEstablishment = 'tipoestablecimiento';
	private $tableSchedule = 'horario';
	
	function __CONSTRUCT($db)
	{
		$this->db = $db;
        $this->response = new Response();
	}

	#Servicios	
	public function registerEstablishment($data){ 
		$email = $this->db->from($this->table)
		                  ->where('email', $data->email)
		                  ->where('idTipoEstablecimiento', $data->idTipoEstablecimiento)
		                  ->fetch(); 

		if($email!=false){
			       $this->response->errors = "Ya existe un establecimiento registrado con esa información";
			return $this->response->SetResponse(false);
		}else{
			$array = array('password' => Cifrado::BLOWFISH($data['password']));
			$new_data = array_merge($data, $array); 

			$register = $this->db->insertInto($this->table, $new_data)
						         ->execute();

			       $this->response->result = $register;
        	return $this->response->SetResponse(true, "Registro exitoso");
		}
	}

	public function informationProfile($id){
		$obtener = $this->db->from($this->table)
							->select(null)
							->select('idEstablecimiento, nombre, urlFoto, urlImageLogo, calificacion, informacionEstablecimiento, direccion, password, email, telefono, idTipoEstablecimiento, idStatusEstablecimiento, precioEnvio')
							->where('idEstablecimiento', $id)
							->fetch();

		$envio = ['precioEnvio' => floatval($obtener->precioEnvio)];
		$calificacion = ['calificacion' => floatval($obtener->calificacion)];

		$obtener->precioEnvio = $envio['precioEnvio'];
		$obtener->calificacion = $calificacion['calificacion'];

				   $this->response->result = $obtener;		
			return $this->response->SetResponse(true);
	}

	//Listar establecimientos dependiendo del tipo de establecimiento
	 public function toListEstablishment($idTipoEstablecimiento){
    	$data = $this->db->from($this->table)
    					 ->select(null)
    					 ->select('idEstablecimiento,  nombre, urlFoto, urlImageLogo, calificacion, informacionEstablecimiento, direccion, password, email, telefono, idTipoEstablecimiento, idStatusEstablecimiento, precioEnvio')
    					 ->where('idTipoEstablecimiento', $idTipoEstablecimiento)
    					 ->orderBy('idEstablecimiento DESC') #ASC
    					 ->limit(6) #limite de elementos en la paginacion
    					 ->offtset(3) #numero de paginaciones
    					 ->fetchAll();


    		   $this->response->result = ['Data' => $data];
    	return $this->response->SetResponse(true);
    }
    
    public function deleteEstablishment(){
    	$this->db->update($this->table)
				 ->set('idStatusEstablecimiento', 2) #set actualiza la columna indicada por el valor indicado
				 ->where('idEstablecimiento', $id)
				 ->execute();

		return $this->response->SetResponse(true, 'Se ha eliminado exitosamente');
    }

    #Actualizar correo 
	public function updateEmailEstablishmet($data,$id){
	    $buscar = $this->db->from($this->table)
	                       ->where('email', $data['email'])
	                       ->fetch();

	    if ($buscar != true) {
	      $actualizar=$this->db->update($this->table, $data)
	                           ->where('idEstablecimiento',$id)        
	                           ->execute();
	          
	             $this->response->result=$actualizar;
	      return $this->response->SetResponse(true,'Actualización correcta.');
	        
	    }else{
	             $this->response->errors='Correo ligado a otra una cuenta';
	      return $this->response->SetResponse(false);
	    }
    }

    public function updatePassworEstablishment($data, $id){
    	$buscar = $this->db->from($this->table)
    					   ->where('password', $data['password'])
    					   ->fetch();

    	if($buscar != false){
    		 	   $this->response->errors='Error, intentelo de nuevo.';
    		return $this->response->SetResponse(false);
    	}else{
    		$array = array('password' => Cifrado::BLOWFISH($data['password'])); #para encriptar la contraseña
			$new_data = array_merge($data, $array); #array_merge:vincula n numero de arreglos para hacerlo uno solo

			$actualizar = $this->db->update($this->table, $new_data)
								   ->where('idEstablecimiento', $id)
						           ->execute(); #excute(ejecuta la consulta)

	               $this->response->result=$actualizar;
	        return $this->response->SetResponse(true,'Actualización correcta.');
    	}
    }

    #Agregar foto del establecimiento
    

    #agregar listar categorias
    public function listCategories($data){
    	$data = $this->db->from($this->tableTypeEstablishment)
    					 ->select(null)
    					 ->select('idTipoEstablecimiento, descripcion, urlImage')
    					 ->orderBy('idTipoEstablecimiento DESC') #ASC
    					 ->limit(10) #limite de elementos en la paginacion
    					 ->offtset(3) #numero de paginaciones
    					 ->fetchAll();

    		   $this->response->result = ['Data' => $data];
    	return $this->response->SetResponse(true);
    }


}
 ?>
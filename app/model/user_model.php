<?php 
namespace App\Model;

use App\Lib\Response,
	App\Lib\Cifrado;

class UserModel 
{
	private $db;
	private $response;
	private $tablePerson = 'persona';
	private $dirSubida = '/var/www/html/lubo_back_dev/img/'; #local
    private $dirServer = 'http://localhost:8080/lubo_back_dev/img/'; #local
	
	public function __CONSTRUCT($db){
		$this->db = $db;
        $this->response = new Response();
	}

	#-----------------------------------------SERVICIOS----------------------------------------------------------------
	#servicio de Registro
	#Valida que no halla algun otro usuario con la misma informacion 	
	public function registerUser($data){ #data(contiene el body que son los datos que se van a insertar en la tabla)
		$email = $this->db->from($this->tablePerson)
		                  ->where('email', $data->email)
		                  ->where('idTipoUsuario', $data->idTipoUsuario)
		                  ->fetch(); #fetch cuando solo es uno solo

		if($email!=false){
			       $this->response->errors = "Ya un usuario con esa información";
			return $this->response->SetResponse(false);
		}else{
			$array = array('password' => Cifrado::BLOWFISH($data['password'])); #para encriptar la contraseña
			$new_data = array_merge($data, $array); #array_merge:vincula n numero de arreglos para hacerlo uno solo

			$register = $this->db->insertInto($this->tablePerson, $new_data)
						         ->execute(); #excute(ejecuta la consulta)

			       $this->response->result = $register;
        	return $this->response->SetResponse(true, "Registro exitoso");
        	#falta mostrar la fecha actual del registro
		}
	}

	public function informationUser($id){
		$obtener = $this->db->from($this->tablePerson)
							->select(null)
						    ->select('nombre, apellidoMaterno, apellidoPaterno, email, password, urlFoto, telefono')
						    ->where('idUsuario', $id)
						    ->fetch();

				   $this->response->result = $obtener;	
			return $this->response->SetResponse(true);
	}

	public function updateInformationUser($data,$idUsuario){
	    $buscar = $this->db->from($this->tablePerson)
	                     ->where('idUsuario', $idUsuario)
	                     ->fetch();

	    if ($buscar != true) {
	             $this->response->errors='Usuario no existe.';
	      return $this->response->SetResponse(false);
	    }else{
	    	 $actualizar = $this->db->update($this->tablePerson, $data) 
	                                ->where('idUsuario',$idUsuario)          
	                                ->execute();

	        if ($actualizar==true) {
	                 $this->response->result=$actualizar;
	          return $this->response->SetResponse(true,'Actualización correcta.');
	        }else{
	                 $this->response->errors='No se pudo actualizar.';
	          return $this->response->SetResponse(false); 
	        }
	    }
    }

	public function deleteUser($id){
		$this->db->update($this->tablePerson)
				 ->set('idstatusPersona', 2) #set actualiza la columna indicada por el valor indicado
				 ->where('idUsuario', $id)
				 ->execute();

		return $this->response->SetResponse(true, 'Se ha eliminado exitosamente');
	}

	public function updateEmailUser($data,$id){
	    $buscar = $this->db->from($this->tablePerson)
	                       ->where('email', $data['email'])
	                       ->fetch();

	    if ($buscar != false) {
	             $this->response->errors='Correo ligado a otra una cuenta';
	      return $this->response->SetResponse(false);
	    }else{
	        $actualizar=$this->db->update($this->tablePerson, $data)
	                             ->where('idUsuario',$id)        
	                             ->execute();
	          
	               $this->response->result=$actualizar;
	        return $this->response->SetResponse(true,'Actualización correcta.');
	    }
    }
 
    public function toList(){
    	$data = $this->db->from($this->tablePerson)
    					 ->select(null)
    					 ->select('idUsuario, nombre, apellidoPaterno, apellidoMaterno, email, password, sexo, urlFoto, telefono, idTipoUsuario, idstatusPersona')
    					 ->orderBy('idUsuario DESC') #ASC
    					 ->limit(6) #limite de elementos en la paginacion
    					 ->offtset(3) #numero de paginaciones
    					 ->fetchAll();

    	$total = $this->db->from($this->tablePerson)
    					  ->select('COUNT(*) total')
    					  ->limit(6) #limite de elementos en la paginacion
    					  ->offtset(3) #numero de paginaciones
    					  ->fetch()
    					  ->total;

    			$this->response->result = ['Data' => $data, 'Total' => $total];
    	return $this->response->SetResponse(true);
    }
 
    #PENDIENTE---------------------------
    #Mostrar foto de perfil
    public function obtainPhotoUser($id){
    	$obtener = $this->db->from($this->tablePerson)
    						->select(null)
    						->select("urlFoto")
                        	->where('idUsuario', $id)
	                        ->limit(1)
	                        ->fetch();

	    if ($obtener != false) {
	             $this->response->result=$obtener;
	      return $this->response->SetResponse(true);
	    }else{
	             $this->response->errors='Error al cargar imagen.';
	      return $this->response->SetResponse(false);
	    }
    }

    #Actualizar contraseña 
    public function updatePassword($data, $id){
    	 $buscar = $this->db->from($this->tablePerson)
	                        ->where('idUsuario', $id)
	                        ->fetch();

	    if ($buscar == false) {
	    	       $this->response->errors='Error al actualizar, intente de nuevo.';
	        return $this->response->SetResponse(false);
	    }else{
	        $array = array('password' => Cifrado::BLOWFISH($data['password'])); #para encriptar la contraseña

			$actualizar = $this->db->update($this->tablePerson, $array)
								   ->where('idUsuario', $id)
						           ->execute(); #excute(ejecuta la consulta)

	               $this->response->result=$actualizar;
	        return $this->response->SetResponse(true,'Actualización correcta.');
	    }
    }

    

    #Recuperar contraseña
    public function recoverPassword(){
    	
    }		
  }
  
 ?>
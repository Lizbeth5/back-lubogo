<?php 
namespace App\Model;

use App\Lib\Response,
	App\Lib\Cifrado;

/**
 * 
 */
class ProductModel
{
	private $db;
	private $response;
	private $tableProduct = 'producto';
	private $tableDetailsProduct = 'detalleproducto';
	
	function __CONSTRUCT($db)
	{
		$this->db = $db;
		$this->response = new Response();
	}

	#Servicios
	public function registerProduct($data){
		$buscar = $this->db->from($this->tableProduct)
		                   ->where('producto', $data['producto'])
		                   ->where('idEstablecimiento', $data['idEstablecimiento'])
		                   ->fetch();

		if($buscar!=false){
			       $this->response->errors = "Ya hay un producto con esta información.";
			return $this->response->SetResponse(false);
		}else{
			$register = $this->db->insertInto($this->tableProduct, $data)
						         ->execute();

			       $this->response->result = $register;
        	return $this->response->SetResponse(true, "El producto se a agregado exitosamente");
		}
	}

	public function informationProduct($id){
		$obtener = $this->db->from($this->tableProduct)
							->select(null)
							->select('idProducto, producto, marca, descripcion, calificacion, precio, urlFoto, descuento, stock, fechaCaducidad, idSubCategorias, idstatusProducto, idEstablecimiento')
							->where('idProducto', $id)
							->fetch();

				   $this->response->result = $obtener;		
			return $this->response->SetResponse(true);
	}

	public function updateInformationProduct($data,$id){
	    $buscar = $this->db->from($this->tableProduct)
	                       ->where('idProducto',$id)
	                       ->fetch();

	    if ($buscar != true) {
	             $this->response->errors='No se encontro el producto.';
	      return $this->response->SetResponse(false);
	    }else{
	    	 $actualizar = $this->db->update($this->tableProduct, $data) 
	                                ->where('idProducto',$id)          
	                                ->execute();

	        if ($actualizar==true) {
	                 $this->response->result=$actualizar;
	          return $this->response->SetResponse(true,'Actualización correcta.');
	        }else{
	                 $this->response->errors='No se pudo actualizar.';
	          return $this->response->SetResponse(false); 
	        }
	    }
    }

	public function deleteProduct($id){
		$this->db->update($this->tableProduct)
				 ->set('idstatusProducto', 2) 
				 ->where('idProducto', $id)
				 ->execute();

		return $this->response->SetResponse(true, 'Se ha eliminado exitosamente');
	}	

	#Mostrar foto de perfil
    public function obtainImage($id){
    	$obtener = $this->db->from($this->tableProduct)
    						->select(null)
    						->select('urlFoto')
                        	->where('idProducto', $id)
	                        ->limit(1)
	                        ->fetch();

	    if ($obtener != false) {
	             $this->response->result=$obtener;
	      return $this->response->SetResponse(true);
	    }else{
	             $this->response->errors='este producto no tiene ninguna imagen';
	      return $this->response->SetResponse(false);
	    }
    }

    public function toListProductsEstablishment($id){
		$obtener = $this->db->from($this->tableProduct)
						->select('marca, descripcion, precio,urlFoto, stock')
						->where('idEstablecimiento', $id)
						->fetchAll();

		foreach ($obtener as $key => $value) {
			$precio = ['precio' => floatval($obtener[$key]->precio)];
			$descuento = ['descuento' => floatval($obtener[$key]->descuento)];
			$stock = ['stock' => floatval($obtener[$key]->stock)];
			$calificacion = ['calificacion' => floatval($obtener[$key]->calificacion)];

			$obtener[$key]->precio = $precio['precio'];
			$obtener[$key]->descuento = $descuento['descuento'];
			$obtener[$key]->stock = $stock['stock'];
			$obtener[$key]->calificacion = $calificacion['calificacion'];
		}

				   $this->response->result = $obtener;	
			return $this->response->SetResponse(true);
	}

	public function listDetailsProducts($idProducto){
		$obtener = $this->db->from($this->tableDetailsProduct)
							->select('idDetalleProducto, Complemento, precioComplemento')
							->where('idProducto', $idProducto)
							->fetchAll();

			   $this->response->result = $obtener;
		return $this->response->SetResponse(true);
	}

	


}
 ?>
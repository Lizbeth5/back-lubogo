<?php 
namespace App\Model;

use App\Lib\Response,
	App\Lib\Cifrado;

/**
 * 
 */
class OffertModel
{
	private $db;
	private $response;
	private $table = 'producto';
	private $tableOffert = 'oferta';
	
	function __CONSTRUCT($db)
	{
		$this->db = $db;
		$this->response = new Response();
	}

	#Servicios
	public function obtainOfferts(){
		$obtenerOffert = $this->db->from($this->tableOffert)
						      ->select('producto.marca, producto.descripcion, producto.calificacion, producto.precio, producto.urlFoto, producto.stock, establecimiento.idEstablecimiento, establecimiento.nombre')
					          ->leftJoin('producto ON producto.idProducto = oferta.idProducto')#primero el nombre de la tabla a dirigir (compara fk de la tabla principal con la fk de  la tabla secundaria)
					          ->leftJoin('establecimiento ON establecimiento.idEstablecimiento = producto.idEstablecimiento')
						      ->fetchAll();

					$this->response->result = $obtenerOffert;
			return $this->response->SetResponse(true);
	}
}
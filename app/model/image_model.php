<?php 
namespace App\Model;

use App\Lib\Response,
	App\Lib\Cifrado;

class ImageModel 
{
	private $db;
	private $response;
	private $tablePerson = 'persona';
	private $tableProduct = 'producto';
	
	
	public function __CONSTRUCT($db){
		$this->db = $db;
        $this->response = new Response();
	}

	#Agregar foto de perfil
    public function addPhotoUser($file, $id){
    	$buscar = $this->db->from($this->tablePerson)
    					   ->where('idUsuario', $id)
    					   ->fetch();

    	if($buscar!=false){
    		//$dirSubida = 'C:/AppServ/www/back-lubogo/image/imageProfile/'; #'/var/www/html/lubo_back_dev/img/'; #local
        	//$dirServer = 'http://localhost:8080/lubo_back_dev/image/imageProfile/'; #local
            //
           $dirSubida = '/home/stardust007/public_html/lubo.com.mx/go/back-lubogo/image/'; #'/var/www/html/lubo_back_dev/img/'; #servidor
           $dirServer = 'http://lubo.com.mx/go/back-lubogo/image/'; #servidor

    	    //verificar que la imagen no tenga detalles
    		if ($file['image']['error'] > 0 or $file['image']['tmp_name']=="") {

             			$this->response->errors = 'Tenemos problemas al cargar el archivo';
     		 	return $this->response->SetResponse(false);
        	}else{
        		//contruir las rutas de acceso a las imagenes
        		$fichero_subida = $dirSubida . "-" .$id."-". basename($file['image']['name']); 
           		$NombreArchivo = $dirServer . "-"  .$id."-". basename($file['image']['name']);

            	$insertar = $this->db->update($this->tablePerson)
            					 	 ->set('urlFoto', $NombreArchivo)
            					     ->where('idUsuario', $id)
            					      ->execute();

            		if($insertar == 1){
            			move_uploaded_file($file['image']['tmp_name'], $fichero_subida);


            			       $this->response->result = $insertar;
            			return $this->response->SetResponse(true, "Se ha agregado la foto");

           		 }else{
           		 	$this->response->errors = "Error al subir";
			return $this->response->SetResponse(false);
           		 }
        	
       		 }
    	}else{
    			   $this->response->errors = "No se encontro el usuario";
			return $this->response->SetResponse(false);
    	}
    }
}
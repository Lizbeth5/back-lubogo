<?php
namespace App\Model;

use App\Lib\Response,
    App\Lib\Cifrado,
    App\Lib\Auth;

class AuthModel
{
    private $db;
    private $tbUser = 'persona';
    private $tbEstablishment = 'establecimiento';
    private $response;

    public function __CONSTRUCT($db)
    {
        $this->db = $db;
        $this->response = new Response();
    }

    public function loginUser($email, $password, $tipoUser){
        $usuario = $this->db->from($this->tbUser)
                            ->where('email', $email)
                            ->where('password', Cifrado::BLOWFISH($password))
                            ->where('idTipoUsuario', $tipoUser)
                            ->fetch();

        if(is_object($usuario)){
            $nombre = explode(' ', $usuario->nombre)[0];
            $token = Auth::SignIn([
                'idUsuario' => $usuario->idUsuario,
                'nombre' => $usuario->nombre,
                'apellidoMaterno' => $usuario->apellidoMaterno,
                'apellidoPaterno' => $usuario->apellidoPaterno,
                'email' => $usuario->email,
                'sexo' => $usuario->sexo,
                'urlPhoto'=>$usuario->urlFoto,
                'telefono' => $usuario->telefono
            ]);

                    $this->response->result = [
                                        'token' => $token,
                                        'id' => $usuario->idUsuario];

            return $this->response->SetResponse(true);
        }else{
            return $this->response->SetResponse(false, "Credenciales no válidas");
        }
    }

    public function loginAdminEstablishment($correo, $password){
        $usuario = $this->db->from($this->tbEstablishment)
                            ->where('correo', $correo)
                            ->where('password', Cifrado::BLOWFISH($password))
                            ->where('idTipoUsuario','3')
                            ->fetch();
        $lugar = $this->db->from($this->tbPlace)
                            ->fetch();

        if(is_object($usuario)){
            $nombre = explode(' ', $usuario->nombre)[0];
            $nombreL = explode(' ', $lugar->nombre)[0];
            $token = Auth::SignIn([
                'idUsuario' => $usuario->idUsuario,
                'nombre' => $nombre,
                'apellidos' => $usuario->apellidos,
                'telefono' => $usuario->telefono,
                'correo' => $usuario->correo,
                'foto' => $usuario->foto

            ]);

                    $this->response->result = [
                                        'token' => $token,
                                        'id' => $usuario->idUsuario];

            return $this->response->SetResponse(true);
        }else{
            return $this->response->SetResponse(false, "Credenciales no válidas");
        }
    }

    public function getData($token){
        $data = Auth::GetData($token);
        if ($data === "Signature verification failed" || $data == 'Expired token' ) {
                   $this->response->errors='Token incorrecto';
            return $this->response->SetResponse(false);
        }else{
                   $this->response->result=$data;
            return $this->response->SetResponse(true);
        }
    }


}
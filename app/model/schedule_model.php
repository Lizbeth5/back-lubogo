<?php 
namespace App\Model;

use App\Lib\Response,
	App\Lib\Cifrado;

/**
 * 
 */
class ScheduleModel
{
	private $db;
	private $response;
	private $tableSchedule = 'horario';
	
	function __CONSTRUCT($db)
	{
		$this->db = $db;
        $this->response = new Response();
	}

	public function listSchedule($idEstablecimiento){
		$data = $this->db->from($this->tableSchedule)
							->select(null)
							->select('idHorario, dia, horaApertura, horaCierre, idEstablecimiento')
							->where('idEstablecimiento', $idEstablecimiento)
							->orderBy('dia DESC')
							->limit(6) #limite de elementos en la paginacion
    					    ->offtset(3) #numero de paginaciones
    					    ->fetchAll();

    		   $this->response->result = ['Data' => $data];
    	return $this->response->SetResponse(true);
		
	}

}
<?php 
namespace App\Model;

use App\Lib\Response,
	App\Lib\Cifrado;

/**
 * 
 */
class GuardedModel
{
	private $db;
	private $response;
	private $tableSaved = 'guardado';
	
	function __CONSTRUCT($db)
	{
		$this->db = $db;
		$this->response = new Response();
	}

	#Servicios
	public function guarded($data){
		$buscar = $this->db->from($this->tableSaved)
						   ->where('idProducto', $data['idProducto'])
						   ->fetch();
			
			if($buscar != false){
				       $this->response->errors='El producto ya ha sido guardado';
				return $this->response->SetResponse(false);
			}else{
				$guardar = $this->db->insertInto($this->tableSaved, $data)
						    ->execute();

			           $this->response->result=$guardar;
		        return $this->response->SetResponse(true, 'Guardado');
			}	
	}

	public function listSaved($id){
		$data = $this->db->from($this->tableSaved)
							->select(null)
							->select ('guardado.idProducto, producto.producto, producto.descripcion, producto.precio, producto.urlFoto, establecimiento.idEstablecimiento, establecimiento.nombre, tipoestablecimiento.idTipoEstablecimiento, tipoestablecimiento.Descripcion')
							->leftJoin('producto ON producto.idProducto = guardado.idProducto')
						    ->leftJoin('establecimiento ON establecimiento.idEstablecimiento = producto.idEstablecimiento')
                            ->leftJoin('tipoestablecimiento ON tipoestablecimiento.idTipoEstablecimiento = establecimiento.idTipoEstablecimiento')
							->where('idUsuario', $id)
							->orderBy('guardado.idProducto DESC')
							->limit(10) #limite de elementos en la paginacion
    					    ->offtset(3) #numero de paginaciones
    					    ->fetchAll();

    		   $this->response->result = ['Data' => $data];
    	return $this->response->SetResponse(true);
		
	}

    public function deleteElementSaved($id){
        $buscar = $this->db->from($this->tableSaved)
        				   ->where('idProducto', $id)
        				   ->where('idStatusGuardado', 1)
        				   ->fetch();
        
        if($buscar != true){
        		$this->response->errors = 'El producto no se encuentra en la lista';
        	return $this->response->SetResponse(false);
        }else{

    		$eliminado = $this->db->update($this->tableSaved)
				             	  ->set('idStatusGuardado', 2) #set actualiza la columna indicada por el valor indicado
						      	  ->where('idProducto', $id)
						          ->execute();

                    $this->response->result=$eliminado;
		    return $this->response->SetResponse(true, "Producto eliminado.");
        }

    }

}
 ?>
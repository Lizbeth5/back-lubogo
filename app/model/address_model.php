<?php 
namespace App\Model;

use App\Lib\Response,
	App\Lib\Cifrado;

class AddressModel 
{
	private $db;
	private $response;
	private $table = 'direccion';		
	
	function __CONSTRUCT($db)
	{
		$this->db = $db;
        $this->response = new Response();
	}

	#Servicios
	#Servicio para agregar dirección tanto del usuario como del establecimiento
	public function addAddress($data, $id){
		$addAddress = $this->db->insertInto($this->table, $data)
					       ->execute();
	}

	public function obtainAddress($id){
		$obtener = $this->db->from($this->table)
							->where('idDireccion', $id)
							->fech();

				   $this->response->result = $obtener;
			return $this->response->SetResponse(true);
	}

	public function updateAddress($data, $id){
		if ($data['TipoUsuario'] == 1) {
			# Update direccion User
			$actualizar = $this->db->update($this->table, $data)
	    						   ->where('idUsuario', $id)
	    						   ->execute();

	    			$this->response->result=$actualizar;
	     	 return $this->response->SetResponse(true, 'Actualización correcta.');

		}else if($data['TipoUsuario'] == 3){
			# Update direccion Establecemiemto
			$actualizar = $this->db->update($this->table, $data)
	    						   ->where('idEstablecimiento', $id)
	    						   ->execute();

	    			$this->response->result=$actualizar;
	     	 return $this->response->SetResponse(true, 'Actualización correcta.');
		}
	}
	
}
 ?>
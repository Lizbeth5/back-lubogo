<?php
namespace App\Model;

use App\Lib\Response,
    App\Lib\Cifrado,
    App\Lib\Auth;

class CarModel
{
    private $db;
    private $tbUser = 'persona';
    private $tbEstablishment = 'establecimiento';
    private $tableCar = 'carrito';
    private $tableCarDetalle = 'detallesseleccionados';
    private $tableProduct = 'producto';
    private $response;

    public function __CONSTRUCT($db)
    {
        $this->db = $db;
        $this->response = new Response();
    }

    public function addProductCarShopping($idUsuario, $data){
        #Crear el carrito
        #Tomar el id del carrito para utilizarlo par llenar la tabla de detallesseleccionados
        #
        


        $car = $this->db->insertInto($this->tableCar, ['idUsuario' => $idUsuario])
                        ->execute(); 

        foreach ($data as $key => $value) {
            #$detailsProducts = ['idDetalleProducto' => $value->idDetalleProducto, 'idCarrito' => $car, 'unidades' => $value->unidades];
            $detailsProducts = ['idDetalleProducto' => $value['idDetalleProducto'], 'idCarrito' => $car, 'unidades' => $value['unidades']];

            $insertar = $this->db->insertInto($this->tableCarDetalle, $detailsProducts)
                                 ->execute();
        }

                $this->response->result=$car;
         return $this->response->SetResponse(true);
    }

    public function obtainInformationCar(){

    }

    #eliminar prdocuto agregado en el carrito
}
<?php 	
use App\Lib\Auth,
    App\Lib\Response,
    App\Middleware\AuthMiddleware;

$app->group('/image/', function () {
    $this->post('addPhotoUser/{idUsuario}', function ($req, $res, $args) {
      $file = $_FILES;
      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->image->addPhotoUser($file,$args['idUsuario']))
                 );
  });

});
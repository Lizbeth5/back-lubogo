<?php 
use App\Lib\Response,
	App\Middleware\AuthMiddleware;

$app->group('/car/', function(){
	$this->post('addProductCarShopping/{idUsuario}', function ($req, $res, $args){
		return $res->withHeader('Content-type', 'application/json')
		   	 	   ->write(
		   	 	   	 json_encode($this->model->car->addProductCarShopping($args['idUsuario'], $req->getParsedBody()))
		   	 	   );
	});

})
 ?>
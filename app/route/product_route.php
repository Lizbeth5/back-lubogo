<?php 
namespace App\Model;

use App\Lib\Response,
	App\Middleware\AuthMiddleware;

$app->group('/product', function(){
	 $this->post('/registerProduct', function ($req, $res, $args) {
       return $res->withHeader('Content-type','application/json') 
                  ->write(
                    json_encode($this->model->product->registerProduct($req->getParsedBody()))
                 );
    });

	 $this->get('/informationProduct/{id}', function ($req, $res, $args){
	 	return $res->withHeader('Content-type', 'application/json')
	 			   ->write(
	 			   	 json_encode($this->model->product->informationProduct($args['id']))
	 			   );
	 });

	 $this->put('/updateInformationProduct/{id}', function($req, $res, $args){
	 	return $res->withHeader('Content-type', 'application/json')
	 			   ->write(
	 			   	 json_encode($this->model->product->updateInformationProduct($req->getParsedBody(), $args['id']))
	 			   );
	 });

	 $this->put('/deleteProduct/{id}', function($req, $res, $args){
	 	return $res->withHeader('Content-type', 'application/json')
	 			   ->write(
	 			   	 json_encode($this->model->product->deleteProduct($args['id']))
	 			   );
	 });

	 $this->get('/toListProductsEstablishment/{id}', function($req, $res, $args){
	 	return $res->withHeader('Content-type', 'application/json')
	 			   ->write(
	 			   	 json_encode($this->model->product->toListProductsEstablishment($args['id']))
	 			   );
	 });

	  $this->get('/obtainImage/{id}', function($req, $res, $args){
	 	return $res->withHeader('Content-type', 'application/json')
	 			   ->write(
	 			   	 json_encode($this->model->product->obtainImage($args['id']))
	 			   );
	 });

	  $this->get('/listDetailsProducts/{idProducto}', function($req, $res, $args){
	  	return $res->withHeader('Content-type', 'application/json')
	  			   ->write(
	  			   	json_encode($this->model->product->listDetailsProducts($args['idProducto']))
	  			   );
	  });


});
 ?>
<?php 	
use App\Lib\Auth,
    App\Lib\Response,
    App\Middleware\AuthMiddleware;

$app->group('/user/', function () {
    $this->post('registerUser', function ($req, $res, $args) {
       return $res->withHeader('Content-type','application/json') 
                  ->write(
                    json_encode($this->model->user->registerUser($req->getParsedBody()))
                 );
    });

    $this->get('informationUser/{id}', function($req, $res, $args){
    	return $res->withHeader('Content-type', 'application/json')
    			   ->write(
    			   	 json_encode($this->model->user->informationUser($args['id']))
    			 );
    });

    $this->put('updateInformationUser/{idUsuario}', function($req, $res, $args){
    	return $res->withHeader('Content-type', 'application/json')
    			   ->write(
    			     json_encode($this->model->user->updateInformationUser($req->getParsedBody(), $args['idUsuario']))
    			 );
    });

    $this->put('deleteUser/{id}', function($req, $res, $args){
    	return $res->withHeader('Content-type', 'application/json')
    			   ->write(
    			   	 json_encode($this->model->user->deleteUser($args['id']))
    			 );
    });

    $this->put('updateEmailUser/{id}', function($req, $res, $args){
        return $res->withHeader('Content-type', 'application/json')
                   ->write(
                     json_encode($this->model->user->updateEmailUser($req->getParsedBody(), $args['id']))
                 );
    });

    #addPhotoUser
    
    $this->get('toList', function($req, $res, $args){
        return $res->withHeader('Content-type', 'application/json')
                   ->write(
                     json_encode($this->model->user->toList($req->getParsedBody()))
                 );
    });

    $this->put('updatePassword/{id}', function($req, $res, $args){
        return $res->withHeader('Content-type', 'application/json')
                   ->write(
                     json_encode($this->model->user->updatePassword($req->getParsedBody(), $args['id']))
                 );
    });

    $this->put('login/{id}', function($req, $res, $args){
        return $res->withHeader('Content-type', 'application/json')
                   ->write(
                     json_encode($this->model->user->login($args['id']))
                 );
    });

    $this->get('obtainPhotoUser/{id}', function($req, $res, $args){
        return $res->withHeader('Content-type', 'application/json')
                   ->write(
                     json_encode($this->model->user->obtainPhotoUser($args['id']))
                   );
    });

});
 ?>
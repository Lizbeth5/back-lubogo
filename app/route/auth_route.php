<?php
use App\Lib\Auth,
    App\Lib\Response,
    App\Validation\userValidation,
    App\Middleware\AuthMiddleware;

$app->group('/auth/', function () {
    $this->post('loginUser', function ($req, $res, $args) {
      $parametros = $req->getParsedBody();
        return $res->withHeader('Content-type','application/json')
                   ->write(
                    json_encode($this->model->auth->loginUser($parametros['email'],$parametros['password'],  $parametros['idTipoUsuario']))
                   );
    });

    $this->post('loginAdminEstablishment', function ($req, $res, $args) {
      $parametros = $req->getParsedBody();
        return $res->withHeader('Content-type','application/json')
                   ->write(
                    json_encode($this->model->auth->loginAdminEstablishment($parametros['correo'],$parametros['password'],  $parametros['idTipoUsuario']))
                   );
    });

    $this->get('getData/{token}', function ($req, $res, $args) {
        return $res->withHeader('Content-type','application/json')
                   ->write(
                    json_encode($this->model->auth->getData($args['token']))
                   );
    });
    

});#->add(new Middleware($app)); #valida que en todas la validaciones en la cabecera se envie un token y va a validar dicho token
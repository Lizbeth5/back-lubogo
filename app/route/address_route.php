<?php 
use App\Lib\Response,
	App\Middleware\AuthMiddleware;

$app->group('/address/', function(){
	$this->post('addAddress', function ($req, $res, $args){
		return $res->withHeader('Content-type', 'application/json')
		   	 	   ->write(
		   	 	   	 json_encode($this->model->address->addAddress($req->getParsedBody()))
		   	 	   );
	});

	$this->get('obtainAddress/{id}', function ($req, $res, $args){
		return $res->withHeader('Content-type', 'application/json')
				   ->write(
				   	 json_encode($this->model->address->obtainAddress($args['id']))
				   );
	});

	$this->put('updateAddress/{id}', function($req, $res, $args){
		return $res->withHeader('Content-type', 'application/json')
				   ->write(
				   	 json_encode($this->model->address->updateAddress($args['id']))
				   );
	});
})

 ?>
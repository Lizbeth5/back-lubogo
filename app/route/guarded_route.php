<?php 
use App\Lib\Auth,
	App\Lib\Response,
	App\Middleware\AuthMiddleware;

$app->group('/guarded/', function (){
	$this->post('guarded', function ($req, $res, $args){
		return $res->withHeader('Content-type', 'application/json')
		   	 	   ->write(
		   	 	   	 json_encode($this->model->guarded->guarded($req->getParsedBody()))
		   	 	   	);
	});

	 $this->get('listSaved/{id}', function($req, $res, $args){
        return $res->withHeader('Content-type', 'application/json')
                   ->write(
                     json_encode($this->model->guarded->listSaved($args['id']))
                  );
    });	

	 $this->put('deleteElementSaved/{id}', function($req, $res, $args){
        return $res->withHeader('Content-type', 'application/json')
                   ->write(
                     json_encode($this->model->guarded->deleteElementSaved($args['id']))
                  );
    });	

});
 ?>
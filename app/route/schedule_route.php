<?php 
use App\Lib\Response,
    App\Middleware\AuthMiddleware;


 $app->group('/schedule/', function () {
    $this->get('listSchedule/{id}', function($req, $res, $args){
        return $res->withHeader('Content-type', 'application/json')
                   ->write(
                     json_encode($this->model->schedule->listSchedule($args['id']))
                  );
    });
 });
 ?>
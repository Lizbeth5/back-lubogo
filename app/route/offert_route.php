<?php 
namespace App\Model;

use App\Lib\Response,
	App\Middleware\AuthMiddleware;

$app->group('/offert/', function(){
	$this->get('obtainOfferts', function ($req, $res, $args){
		return $res->withHeader('Content-type', 'application/json')
		   	 	   ->write(
		   	 	   	 json_encode($this->model->offert->obtainOfferts())
		   	 	   	);
	});
});
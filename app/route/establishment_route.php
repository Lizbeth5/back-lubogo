<?php 
use App\Lib\Response,
    App\Middleware\AuthMiddleware;


 $app->group('/establishment/', function () {
 	$this->post('registerEstablishment', function ($req, $res, $args) {
       return $res->withHeader('Content-type','application/json') 
                  ->write(
                    json_encode($this->model->establishment->registerEstablishment($req->getParsedBody()))
                  );
    });

  	$this->get('informationProfile/{id}', function($req, $res, $args){
    	return $res->withHeader('Content-type', 'application/json')
    			   ->write(
    			   	 json_encode($this->model->establishment->informationProfile($args['id']))
    			        );
    });

    $this->get('toListEstablishment/{id}', function($req, $res, $args){
        return $res->withHeader('Content-type', 'application/json')
                   ->write(
                     json_encode($this->model->establishment->toListEstablishment($args['id']))
                  );
    });

    $this->put('deleteEstablishment/{id}', function($req, $res, $args){
    	return $res->withHeader('Content-type', 'application/json')
    			   ->write(
    			   	 json_encode($this->model->establishment->deleteEstablishment($args['id']))
    			        );
    });

    $this->put('updateEmailEstablishmet/{id}', function($req, $res, $args){
    	return $res->withHeader('Content-type', 'application/json')
    			       ->write(
    			   	    json_encode($this->model->establishment->updateEmailEstablishmet($req->getParsedBody(), $args['id']))
    			        );
    });    

    $this->put('updatePassworEstablishment/{id}', function($req, $res, $args){
       return $res->withHeader('Content-type', 'application/json')
                  ->write(
                    json_encode($this->model->establishment->updatePassworEstablishment($req->getParsedBody(), $args['id']))
                  );
    });

    $this->get('listCategories', function($req, $res, $args){
      return $res->withHeader('Content-type', 'application/json')
                 ->write(
                    json_encode($this->model->establishment->listCategories($req->getParsedBody()))
                 );
    });

 });
 ?>